package demojpa.service;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNotNull;
import static org.mockito.Mockito.lenient;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

import java.lang.reflect.Field;
import java.math.BigInteger;

import javax.persistence.EntityManager;
import javax.persistence.EntityTransaction;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;

import demojpa.domain.Account;
import demojpa.repository.AccountRepository;

@ExtendWith(MockitoExtension.class)
public class BankingTest {

    @Mock
    private EntityManager em;

    @Mock
    private AccountRepository accountRepository;

    @Mock
    private EntityTransaction transaction;

    @InjectMocks
    private Banking banking = new Banking();

    @BeforeEach
    public void init() {
        lenient().when(banking.em.getTransaction()).thenReturn(transaction);
    }

    @Test
    public void when_createIsCalledWithValidAccount_then_ReturnAccountWithId(){
        Long id = Long.valueOf(77);
        Account account = new Account("A3", BigInteger.valueOf(33));
        Account accountSaved = new Account("A3", BigInteger.valueOf(33));
        // accountSaved.setId(id);
        setAccountId(accountSaved, id);
        when(accountRepository.save(account)).thenReturn(accountSaved);
        // when(banking.em.getTransaction()).thenReturn(transaction);

        
        Account created = banking.create(account);

        verify(accountRepository).save(account);
        assertNotNull(created.getId());
        assertEquals(id, created.getId());
        
    }

    @Test
    public void when_transferIsCalledWithSufficientBalance_then_depositIsCalled(){
        BigInteger amount = BigInteger.valueOf(10);
        Long sourceId = Long.valueOf(33);
        Long targetId = Long.valueOf(77);
        Account accountSource = new Account("source33", BigInteger.valueOf(33));
        Account accountTarget = new Account("target77", BigInteger.valueOf(77));
        when(accountRepository.findById(sourceId)).thenReturn(accountSource);
        when(accountRepository.findById(targetId)).thenReturn(accountTarget);
        
        banking.transferMoney(sourceId, targetId, amount);

        verify(accountRepository).update(accountSource);
        verify(accountRepository).update(accountTarget);

    

    }




    private Account setAccountId(Account account, Long id){
        Field idField;
        try{
            idField = account.getClass().getDeclaredField("id");
            idField.setAccessible(true);
            idField.set(account, id);
        } catch (Exception e){

        }
        return account;
        

    }
    
}
