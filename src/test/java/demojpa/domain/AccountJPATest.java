package demojpa.domain;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.Persistence;
import javax.persistence.Query;
import java.math.BigInteger;

import static org.junit.jupiter.api.Assertions.*;

public class AccountJPATest {
    /*
    quick check for annotations
     */

    EntityManagerFactory emf = Persistence.createEntityManagerFactory("h2");
    EntityManager em;

    @BeforeEach
    public void setUp() {
        cleanUp();
        em = emf.createEntityManager();
    }


    @Test
    void saveAccount(){
        Long cResultsBefore = accountCount();
        Account a1 = new Account("a1", BigInteger.valueOf(1000));
        assertNull (a1.getId());
        em.getTransaction().begin();
        em.persist(a1);
        em.getTransaction().commit();
        assertNotNull(a1.getId());
        Long cAfter = accountCount();

        assertEquals(1L, cAfter - cResultsBefore);
    }


    @Test
    void add2Accounts() {

        Long cResultsBefore = accountCount();
        Account a1 = new Account("a1", BigInteger.valueOf(1000));
        Account a2 = new Account("a2", BigInteger.valueOf(2000));
        assertNull(a1.getId());
        em.getTransaction().begin();
        em.persist(a1);
        em.persist(a2);
        em.getTransaction().commit();

        Long cAfter = accountCount();

        assertEquals(2L, cAfter - cResultsBefore);
    }


    private Long accountCount() {
        EntityManager em = emf.createEntityManager();
        Query query = em.createQuery("SELECT COUNT (a.id)  FROM Account a");
        return (Long) query.getSingleResult();
    }

    private void cleanUp() {
        EntityManager em = emf.createEntityManager();
        Query query = em.createQuery("DELETE FROM Account a");
        em.getTransaction().begin();
        query.executeUpdate();
        em.getTransaction().commit();
    }

}
