package demojpa.domain;

import org.junit.jupiter.api.Test;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.ValueSource;

import static org.junit.jupiter.api.Assertions.*;

import java.math.BigInteger;

class AccountTest {

    @ParameterizedTest
    @ValueSource(ints = {1, 2, 99, 100})
    void mayWithDraw(int number) {
        Account a1 = new Account("a1", BigInteger.valueOf(100));
        BigInteger bigVal = BigInteger.valueOf(number);
        assertTrue(a1.mayWithdraw(bigVal));
    }


    @ParameterizedTest
    @ValueSource(ints = {0, -1, 101})
    void mayNotWithDraw(int number) {
        Account a1 = new Account("a1", BigInteger.valueOf(100));
        BigInteger bigVal = BigInteger.valueOf(number);
        assertFalse(a1.mayWithdraw(bigVal));
    }

    @Test
    void depositIsCorrect() {
        Account a1 = new Account("a1", BigInteger.valueOf(1000));
        a1.deposit(BigInteger.valueOf(99));
        BigInteger expected = BigInteger.valueOf(1099);
        assertEquals(expected, a1.getBalance());
    }


    @Test
    void withdrawIsCorrect(){
        Account a1 = new Account("a1", BigInteger.valueOf(1000));
        a1.withdraw(BigInteger.valueOf(99));
        BigInteger expected = BigInteger.valueOf(901);
        assertEquals(expected, a1.getBalance());
    }


}