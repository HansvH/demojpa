package demojpa.resource;

import demojpa.domain.Account;

import java.math.BigInteger;

public class AccountDTO {

    private Long id;
    private String accountNr;
    private BigInteger balance;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getAccountNr() {
        return accountNr;
    }

    public void setAccountNr(String accountNr) {
        this.accountNr = accountNr;
    }

    public BigInteger getBalance() {
        return balance;
    }

    public void setBalance(BigInteger balance) {
        this.balance = balance;
    }

    public Account convert2Account(){
        return new Account(accountNr, balance);
    }
}
