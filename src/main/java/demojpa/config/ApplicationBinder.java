package demojpa.config;


import demojpa.repository.AccountRepository;
import demojpa.repository.AccountRepositoryJPA;
import demojpa.service.Banking;
import org.glassfish.hk2.utilities.binding.AbstractBinder;

public class ApplicationBinder extends AbstractBinder {
    @Override
    protected void configure() {
        bind(Banking.class).to(Banking.class);
        bind(AccountRepositoryJPA.class).to(AccountRepository.class);

    }
}
