package demojpa.repository;

import demojpa.domain.Account;

import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.Persistence;
import java.util.List;

public class AccountRepositoryJPA implements AccountRepository{

    private EntityManager em;

    public void setEm(EntityManager em) {
        this.em = em;
    }

    @Override
    public List<Account> allAccounts() {

        List<Account> result = em.createQuery("from Account ", Account.class).getResultList();

        return result;
    }

    @Override
    public Account save(Account account) {
        em.persist(account);
        return account;
    }

    @Override
    public Account update(Account account) {
        Account merged = em.merge(account);
        return merged;
    }

    @Override
    public Account findById(Long id) {
        Account account = em.find(Account.class, id);
        return account;
    }
}
