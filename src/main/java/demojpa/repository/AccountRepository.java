package demojpa.repository;

import demojpa.domain.Account;

import javax.persistence.EntityManager;
import java.util.List;

public interface AccountRepository {

    void setEm(EntityManager em);

    List<Account> allAccounts();

    Account save(Account account);

    Account update(Account account);

    Account findById(Long id);
}
