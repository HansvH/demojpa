package demojpa.repository;

import demojpa.domain.Account;

import javax.persistence.EntityManager;
import java.math.BigInteger;
import java.util.ArrayList;
import java.util.List;

public class AccountRepositoryList implements AccountRepository {

    @Override
    public void setEm(EntityManager em) {

    }

    @Override
    public List<Account> allAccounts() {
        List<Account> accounts = new ArrayList<>();
        Account a1 = new Account("a1", BigInteger.valueOf(100));
        Account a2 = new Account("a2", BigInteger.valueOf(200));
        accounts.add(a1);
        accounts.add(a2);
        return accounts;
    }

    @Override
    public Account save(Account account) {
        //todo implement
        return account;
    }

    @Override
    public Account update(Account account) {
        //todo implement
        return account;
    }

    @Override
    public Account findById(Long id) {
        //todo implement
        return null;
    }


}
