package demojpa.service;

import demojpa.domain.Account;
import demojpa.repository.AccountRepository;

import java.math.BigInteger;
import java.util.List;

import javax.annotation.PostConstruct;
import javax.inject.Inject;
import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.Persistence;



public class Banking {

    private static final EntityManagerFactory emf = Persistence.createEntityManagerFactory("bankPU");
    EntityManager em = emf.createEntityManager();

    @Inject
    private AccountRepository accountRepository;

    @PostConstruct
    private void init(){
        accountRepository.setEm(em);
    }


    public List<Account> allAccounts() {
        return accountRepository.allAccounts();
    }

    public Account create(Account account) {
        em.getTransaction().begin();
        accountRepository.save(account);
        em.getTransaction().commit();
        em.close();
        return account;

    }

    public Account findById(Long id) {
        return accountRepository.findById(id);
    }

    public boolean withDraw(Account account, BigInteger amount) {
        boolean success = account.withdraw(amount);
        if (success) accountRepository.update(account);
        return success;
    }

    public boolean deposit(Account account, BigInteger amount) {
        boolean success = account.deposit(amount);
        if (success) accountRepository.update(account);
        return success;
    }


    public boolean transferMoney(Long sourceId, Long targetId, BigInteger  money){

        em.getTransaction().begin();
        Account sourceAccount = findById(sourceId);
        Account targetAccount = findById(targetId);


        // ToDo fix next line as to correct rollback
        boolean withdrawalSucceeded = withDraw(sourceAccount, money);
        if (withdrawalSucceeded){
            // if (true) throw new RuntimeException();  // simulates breakdown
            boolean deposit = deposit(targetAccount, money);
            em.getTransaction().commit();
            em.close();
            return deposit;
        }
        em.getTransaction().rollback();
        em.close();
        return false;
    }

}
