package demojpa.domain;

import javax.persistence.*;
import java.math.BigInteger;

@Entity
public class Account {

    @Id @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;
    private String accountNr;
    private BigInteger balance;

    public Account() {
    }

    public Account(String accountNr, BigInteger balance) {
        this.accountNr = accountNr;
        this.balance = balance;
    }

    public boolean deposit(BigInteger money){
        if(isNegative(money)) return false;
        balance = balance.add(money);
        return true;
    }

    public boolean withdraw(BigInteger money){
        if( !mayWithdraw(money)) return false;
        balance = balance.subtract(money);
        return true;
    }


    public String getAccountNr() {
        return accountNr;
    }

    public BigInteger getBalance() {
        return balance;
    }

    public Long getId() {
        return id;
    }

    // public void setId(Long id) {
    //     this.id = id;
    // }

    //helper methods
    public boolean mayWithdraw(BigInteger amount) {
        BigInteger resultingBalance = balance.subtract(amount);
        return isPositive(amount) && isPositiveOrZero(resultingBalance);
    }

    public boolean isPositiveOrZero(BigInteger amount) {
        return amount.compareTo(BigInteger.ZERO) >= 0;
    }

    public boolean isNegative(BigInteger amount) {
        return amount.compareTo(BigInteger.ZERO) < 0;
    }

    public boolean isPositive(BigInteger amount) {
        return amount.compareTo(BigInteger.ZERO) > 0;
    }

}
